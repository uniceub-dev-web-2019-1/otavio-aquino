package handle

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// hearderToString converts http headers into string
//
func headerToString(head http.Header) string {
	var theStr string

	for key, value := range head {
		theStr += key + ": " + fmt.Sprint(value) + "\n"
	}

	return theStr
}

// My404Handler is the basic handler for not found requests
//
func My404Handler() http.Handler {
	return http.HandlerFunc(my404Dealer)
}

// My405Handler is the basic handler for not allowed requests
//
func My405Handler() http.Handler {
	return http.HandlerFunc(my405Dealer)
}

func my404Dealer(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")
	status := http.StatusNotFound
	respJSON := map[string]interface{}{
		"status":     status,
		"statusText": fmt.Sprintf("%d: %s", status, http.StatusText(status)),
		"message":    "Could not find path",
		"path":       req.RequestURI,
	}
	byteBody, _ := json.Marshal(respJSON)
	resp.WriteHeader(status)
	resp.Write(byteBody)
}

func my405Dealer(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")
	status := http.StatusMethodNotAllowed
	respJSON := map[string]interface{}{
		"status":     status,
		"statusText": fmt.Sprintf("%d: %s", status, http.StatusText(status)),
		"message":    "This method is not allowed to this path",
		"path":       req.RequestURI,
	}
	byteBody, _ := json.Marshal(respJSON)
	resp.WriteHeader(status)
	resp.Write(byteBody)
}
