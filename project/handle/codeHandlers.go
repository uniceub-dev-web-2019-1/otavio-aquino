package handle

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"otavio-aquino/project/db"
	"otavio-aquino/project/structs"
	"otavio-aquino/project/validator"
)

var langs, _ = json.Marshal(validator.Langs)

// PostCodeHandler validates code creation
//
func PostCodeHandler(resp http.ResponseWriter, req *http.Request) {
	var ccod structs.CodeCheck
	var code structs.Code
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	statuscode, valErr, err := validator.ValidateBody(req.Body, &ccod)

	respJSON := map[string]interface{}{
		"status": statuscode,
		"error":  err,
		"errors": valErr,
		"code":   nil,
	}

	if err != nil {
		log.Printf("%v\n\n", err)
		byteBody, _ := json.Marshal(respJSON)

		resp.WriteHeader(statuscode)
		resp.Write(byteBody)
		return
	}

	user, err := db.SearchUser("email", ccod.User)

	if user == (structs.User{}) || user.Email != ccod.User || ccod.Token != user.GenToken() {

		statuscode = http.StatusUnauthorized
		respJSON["status"] = statuscode
		respJSON["error"] = "Invalid User"
		byteBody, _ := json.Marshal(respJSON)
		resp.Header().Set("WWW-Authenticate", "Basic realm=\"Restricted\"")

		resp.WriteHeader(statuscode)
		resp.Write(byteBody)
		return
	}

	ins := structs.UserInfo{
		Name:   user.Name,
		Nick:   user.Nick,
		Email:  user.Email,
		Points: user.Points,
		Rep:    user.Rep,
	}

	if err != nil {
		log.Print(err)
	}

	code = structs.Code{
		ID:    genID(),
		Title: ccod.Title,
		Code:  ccod.Code,
		Lang:  ccod.Lang,
		User:  ins,
	}

	// resp.Write([]byte(fmt.Sprintf("Created code %v", code)))

	log.Print(fmt.Sprintf("%d %s Created Code: %s\n\n", http.StatusOK, http.StatusText(http.StatusOK), code.Title))
	db.InsertCode(code)
	respJSON["code"] = code
	jsponse, _ := json.Marshal(respJSON)

	resp.Write(jsponse)
}

// LookForCodes looks for odes
//
func LookForCodes(resp http.ResponseWriter, req *http.Request) {
	nick := req.FormValue("nick")
	lang := req.FormValue("lang")

	resp.Header().Set("Content-Type", "application/json; charset=utf-8")
	var codes []structs.Code

	if nick == "" && lang == "" {
		resp.Write([]byte("[]"))
		return
	}
	if nick != "" {
		codes = db.FindCodes("user.nick", nick)
	} else if lang != "" {
		codes = db.FindCodes("lang", lang)
	}

	cb, err := json.Marshal(codes)
	if err != nil {
		log.Print(err)
	}
	resp.Write(cb)
}

// PutCodeHandler validates if code can be updated
//
func PutCodeHandler(resp http.ResponseWriter, req *http.Request) {
	var codu structs.CodeUpdate
	var code structs.Code
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	statuscode, errors, err := validator.ValidateBody(req.Body, &codu)
	respJSON := map[string]interface{}{
		"error":  err,
		"errors": errors,
		"status": statuscode,
		"code":   nil,
		"msg":    "",
	}
	if err != nil {
		log.Printf("%v\n\n", err)
		byteBody, _ := json.Marshal(respJSON)
		resp.WriteHeader(statuscode)
		resp.Write(byteBody)
		return
	}

	code = db.FindCode(codu.ID)
	user, err := db.SearchUser("email", code.User.Email)

	if code.User.Email != codu.User || codu.Token != user.GenToken() {
		statuscode = http.StatusForbidden
		log.Print("user changing other user info")
		respJSON["status"] = statuscode
		respJSON["error"] = "User cant change other user info"
		byteBody, _ := json.Marshal(respJSON)
		resp.WriteHeader(statuscode)
		resp.Write(byteBody)
		return
	}

	if err != nil {
		log.Print(err)
	}

	log.Print(fmt.Sprintf("%d %s Updating Code Number: %d\n\n", http.StatusOK, http.StatusText(http.StatusOK), codu.ID))
	err = db.UpdateCode(codu)
	if err != nil {
		log.Print(err)
		statuscode = http.StatusInternalServerError
		respJSON["status"] = statuscode
		respJSON["msg"] = "something went wrong trying to update"
		respJSON["error"] = "something went wrong trying to update"
		byteBody, _ := json.Marshal(respJSON)
		resp.WriteHeader(statuscode)
		resp.Write(byteBody)
		return
	}

	code.Code = codu.Code
	code.Lang = codu.Lang
	code.Title = codu.Title
	respJSON["code"] = code
	bbod, _ := json.Marshal(respJSON)
	resp.Write(bbod)
}

// DelCodeHandler validates if code can be deleted
//
func DelCodeHandler(resp http.ResponseWriter, req *http.Request) {
	var codd structs.CodeDelete
	var code structs.Code
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	statuscode, errors, err := validator.ValidateBody(req.Body, &codd)
	respJSON := map[string]interface{}{
		"errors":  errors,
		"status":  statuscode,
		"error":   err,
		"oldcode": nil,
		"msg":     "",
	}

	if err != nil {
		log.Printf("%v\n\n", err)
		byteJ, _ := json.Marshal(respJSON)
		resp.WriteHeader(statuscode)
		resp.Write(byteJ)
		return
	}

	code = db.FindCode(codd.ID)

	if code == (structs.Code{}) {
		statuscode = http.StatusNotFound
		log.Print("no codes were found")
		respJSON["status"] = statuscode
		respJSON["error"] = "no codes were found"
		byteJ, _ := json.Marshal(respJSON)
		resp.WriteHeader(statuscode)
		resp.Write(byteJ)
		return
	}

	user, err := db.SearchUser("email", code.User.Email)

	if code.User.Email != codd.User || codd.Token != user.GenToken() {
		log.Print("user changing other user info")
		statuscode = http.StatusForbidden
		respJSON["status"] = statuscode
		respJSON["error"] = "An user is trying to change other user info"
		byteJ, _ := json.Marshal(respJSON)
		resp.WriteHeader(statuscode)
		resp.Write(byteJ)
		return
	}

	if err != nil {
		log.Print(err)
	}

	err = db.DeleteCode(codd)
	if err != nil {
		log.Print(err)
		respJSON["status"] = 500
		respJSON["msg"] = "Something went wrong trying to delete"
		respJSON["error"] = err

		byteJ, _ := json.Marshal(respJSON)
		resp.WriteHeader(http.StatusInternalServerError)
		resp.Write(byteJ)
		return
	}

	log.Print(fmt.Sprintf("%d %s Deleted Code Number: %d\n\n", http.StatusOK, http.StatusText(http.StatusOK), codd.ID))

	respJSON["msg"] = "O código foi deletado com sucesso!"
	respJSON["oldcode"] = code
	bbod, _ := json.Marshal(respJSON)
	resp.Write(bbod)
}

// ShowLangs returns all supported languages
//
func ShowLangs(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")
	resp.Write(langs)
}

func genID() int64 {
	return db.LastCodeID() + 1
}
