package handle

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"otavio-aquino/project/db"
	"otavio-aquino/project/structs"
	"otavio-aquino/project/validator"
)

// PostUserHandler validates user creation
//
func PostUserHandler(resp http.ResponseWriter, req *http.Request) {
	var user structs.User

	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	statuscode, valErr, err := validator.ValidateBody(req.Body, &user)

	respJSON := map[string]interface{}{
		"status":     statuscode,
		"statusText": http.StatusText(statuscode),
		"errors":     valErr,
	}

	if err != nil {
		log.Printf("[%v]\n\n", err)
		byteResponse, _ := json.Marshal(respJSON)
		resp.WriteHeader(statuscode)
		resp.Write(byteResponse)
		return
	}

	err = db.InsertUser(user)
	if err != nil {
		log.Print(err)
	}

	byteResponse, _ := json.Marshal(respJSON)
	resp.Write(byteResponse)
	log.Print(fmt.Sprintf("%d %s Created User: %s\n\n", statuscode, http.StatusText(statuscode), user.Name))

}

// PutUserHandler ...
//
func PutUserHandler(resp http.ResponseWriter, req *http.Request) {
	var user structs.User
	var upsr structs.UpdatableUser

	statuscode, _, err := validator.ValidateBody(req.Body, &upsr)

	if err != nil {
		log.Printf("[%v]\n\n", err)
		resp.WriteHeader(statuscode)
		return
	}

	err = db.UpdateUser(upsr.OldUser, upsr.NewUser)

	if err != nil {
		log.Print(err)
		return
	}

	if user == (structs.User{}) {
		resp.Write([]byte("No users found"))
		return
	}

}

// LoginUser ...
//
func LoginUser(resp http.ResponseWriter, req *http.Request) {
	var lusr structs.SearchableUser
	var user structs.User
	resp.Header().Set("Content-Type", "application/json; charset=utf-8")

	statuscode, erros, err := validator.ValidateBody(req.Body, &lusr)
	respJSON := map[string]interface{}{
		"status": statuscode,
		"errors": erros,
		"error":  err,
	}

	if err != nil {
		log.Printf("[%v]\n\n", err)

		byteBody, _ := json.Marshal(respJSON)
		resp.WriteHeader(statuscode)
		resp.Write(byteBody)
		return
	}

	user, err = db.SearchUser("email", lusr.Email)

	if lusr.Pass == user.Pass {
		rj := map[string]interface{}{
			"token":  user.GenToken(),
			"name":   user.Name,
			"nick":   user.Nick,
			"email":  user.Email,
			"points": user.Points,
			"rep":    user.Rep,
		}

		respJSON["user"] = rj

		jsonB, _ := json.Marshal(respJSON)

		resp.Write(jsonB)
		return
	}

	respJSON["error"] = "Invalid email or password"
	respJSON["status"] = 400
	jsonB, _ := json.Marshal(respJSON)

	resp.WriteHeader(400)
	resp.Write(jsonB)

}
