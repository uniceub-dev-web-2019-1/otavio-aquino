package utils

// Addresses
const (

	// ServerAddr is the address of the server
	ServerAddr = "0.0.0.0:8080"

	// DBAddr is the address of the Database
	DBAddr = "mongodb://0.0.0.0:27017"
)

// Paths
const (

	// UserPath is the path used to deal with users
	UserPath = "/user"

	// LoginPath is used to login users
	LoginPath = "/login"

	// CodePath is the path userd to deal with codes
	CodePath = "/code"
)

// Names
const (

	// DBName is the name of the database
	DBName = "stackbook"
)
