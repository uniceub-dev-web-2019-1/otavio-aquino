package utils

// InSlice Checks if slice contains value
//
func InSlice(slice []string, value string) bool {

	for _, cVal := range slice {
		if cVal == value {
			return true
		}
	}
	return false
}
