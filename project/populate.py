import json
import requests

def populate(structType):
    with open(f"jsons/{structType}.json") as f:
        structList = json.load(f)
    for struct in structList:
        requests.post(f"http://172.22.87.33:8080/{structType}", json=struct)

populate("user")
populate("code")