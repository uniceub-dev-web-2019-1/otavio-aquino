package db

import (
	"context"
	"log"
	"otavio-aquino/project/structs"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// InsertUser ...
//
func InsertUser(user structs.User) (err error) {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err = db.Collection("users").InsertOne(ctx,
		bson.M{
			"name":   user.Name,
			"nick":   user.Nick,
			"pass":   user.Pass,
			"email":  user.Email,
			"points": user.Points,
			"rep":    user.Rep,
		})

	return
}

// SearchUser ...
//
func SearchUser(field string, val string) (u structs.User, err error) {
	c := db.Collection("users")
	filter := bson.D{{field, val}}

	err = c.FindOne(context.TODO(), filter).Decode(&u)
	if err == mongo.ErrNoDocuments {

		return u, nil
	}
	return
}

// UpdateUser updates an existing user
//
func UpdateUser(find structs.SearchableUser, newUsr structs.User) (err error) {
	c := db.Collection("users")
	filter := bson.D{{"email", find.Email}}

	user, err := SearchUser("email", find.Email)
	if err != nil {
		log.Printf("[%v]\n\n", err)
		return
	}

	if user == (structs.User{}) {
		err = mongo.ErrNoDocuments
		return err
	}

	newUser := bson.D{
		{"name", user.Name},
		{"email", user.Email},
		{"nick", user.Nick},
		{"pass", user.Pass},
		{"points", user.Points},
		{"rep", user.Rep}}

	_, err = c.UpdateOne(context.TODO(), filter, newUser)
	return
}

// GetSize Retusns size
func GetSize() (int64, error) {
	return db.Collection("users").CountDocuments(context.TODO(), bson.D{})
}

// AllUsers returns all users
//
func AllUsers() (users []structs.User, err error) {
	return
}
