package db

import (
	"context"
	"fmt"
	"log"
	"otavio-aquino/project/structs"
	"time"

	"go.mongodb.org/mongo-driver/mongo"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// InsertCode ...
//
func InsertCode(code structs.Code) (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err = db.Collection("codes").InsertOne(ctx,
		bson.M{
			"id":    code.ID,
			"title": code.Title,
			"code":  code.Code,
			"lang":  code.Lang,
			"user":  code.User})

	updateCodeID(code.ID)
	return
}

// AllCodes returns all codes
//
func AllCodes() (codes []structs.Code) {

	c := db.Collection("codes")
	ctx := context.TODO()

	cur, err := c.Find(ctx, bson.D{{}})
	defer cur.Close(ctx)
	for cur.Next(ctx) {
		var code structs.Code

		if err = cur.Decode(&code); err != nil {

			fmt.Print(string(cur.Current))
			log.Fatal(err)
		}

		codes = append(codes, code)
	}

	return
}

// FindCode find a single code based on id
func FindCode(id int64) (code structs.Code) {
	c := db.Collection("codes")
	ctx := context.TODO()

	filter := bson.M{"id": id}
	err := c.FindOne(ctx, filter).Decode(&code)

	if err == mongo.ErrNoDocuments {
		return
	} else if err != nil {
		log.Fatal(err)
	}
	return
}

// FindCodes finds all codes related to the nick
//
func FindCodes(field string, val string) (codes []structs.Code) {
	c := db.Collection("codes")
	ctx := context.TODO()

	filter := bson.M{field: primitive.Regex{Pattern: "^" + val + "$", Options: ""}}

	cur, err := c.Find(ctx, filter)

	for cur.Next(ctx) {
		var code structs.Code
		if err = cur.Decode(&code); err != nil {
			fmt.Print(string(cur.Current))
			log.Fatal(err)
		}
		codes = append(codes, code)
	}
	return
}

// CountCodes counts all codes in database
//
func CountCodes() (amount int64) {
	c := db.Collection("codes")
	ctx := context.TODO()

	amount, err := c.CountDocuments(ctx, bson.D{{}})
	if err != nil {

		log.Fatal(err)
	}
	return amount
}

// UpdateCode updates a code
//
func UpdateCode(upc structs.CodeUpdate) error {
	c := db.Collection("codes")
	filter := bson.D{{"id", upc.ID}}

	// if user == (structs.User{}) {
	// 	err = mongo.ErrNoDocuments
	// 	return err
	// }

	newCode := bson.M{"$set": bson.D{
		{"title", upc.Title},
		//{"code", upc.Code},
		{"lang", upc.Lang},
	}}

	_, err := c.UpdateOne(context.TODO(), filter, newCode)
	return err
}

// LastCodeID returns last id
//
func LastCodeID() int64 {
	c := db.Collection("codeid")

	ctx := context.TODO()

	var codid structs.CodeID

	err := c.FindOne(ctx, bson.D{{}}).Decode(&codid)

	if err == mongo.ErrNoDocuments {
		return -1
	}

	if err != nil {
		log.Fatal(err)
	}

	return codid.LastID

}

// DeleteCode deletes a code
//
func DeleteCode(dtc structs.CodeDelete) error {
	c := db.Collection("codes")

	filter := bson.D{{"id", dtc.ID}}

	_, err := c.DeleteOne(context.TODO(), filter)
	return err
}

func updateCodeID(id int64) {
	c := db.Collection("codeid")
	filter := bson.D{{}}

	newVal := bson.M{"$set": bson.D{{"lastid", id}}}

	_, err := c.UpdateOne(context.TODO(), filter, newVal)

	if err != nil {
		log.Print(err)
	}

}
