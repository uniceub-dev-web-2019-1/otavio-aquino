package db

import (
	"context"
	"log"
	"otavio-aquino/project/utils"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var client *mongo.Client
var db *mongo.Database

// CreateConection creates Connection
//
func CreateConection() (err error) {
	client, err = mongo.NewClient(options.Client().ApplyURI(utils.DBAddr))

	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(ctx)

	db = client.Database(utils.DBName)
	if LastCodeID() == -1 {
		startLastCodeID()
	}
	return
}

func startLastCodeID() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := db.Collection("codeid").InsertOne(ctx,
		bson.M{
			"lastid": 0,
		})
	if err != nil {
		log.Fatal(err)
	}

	return
}
