package structs

import "fmt"

// Code is a basic struct to represent a code
//
type Code struct {
	ID    int64    `json:"id" validate:"required"`
	Title string   `json:"title" validate:"required,max=50"`
	Code  string   `json:"code" validate:"required"`
	Lang  string   `json:"lang" validate:"required,language"`
	User  UserInfo `json:"user" validate:"required"`
}

// CodeID is used to create a new id for codes
//
type CodeID struct {
	LastID int64 `json:"lastid"`
}

// CodeCheck is a basic struct to check	 a code
//
type CodeCheck struct {
	Title string `json:"title" validate:"required,max=50"`
	Code  string `json:"code"  validate:"required"`
	Lang  string `json:"lang"  validate:"required,language"`
	User  string `json:"user"  validate:"required,email"`
	Token string `json:"token" validate:"required"`
}

// CodeUpdate is a basic struct to check if the code is able to be updated
//
type CodeUpdate struct {
	ID    int64  `json:"id" validate:"required"`
	Title string `json:"title" validate:"required,max=50"`
	Code  string `json:"code"  validate:"required"`
	Lang  string `json:"lang"  validate:"required,language"`
	User  string `json:"user"  validate:"required,email"`
	Token string `json:"token" validate:"required"`
}

// CodeDelete is a basic struct to check if the code is able to be deleted
//
type CodeDelete struct {
	ID    int64  `json:"id" validate:"required"`
	User  string `json:"user"  validate:"required,email"`
	Token string `json:"token" validate:"required"`
}

func (c Code) String() string {
	return fmt.Sprintf("%s\n%s\nLanguage: %s", c.Title, c.Code, c.Lang)

}
