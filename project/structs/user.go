package structs

import "fmt"

// User is a basic struct to represent user
//
type User struct {
	Name   string `json:"name" validate:"required"`
	Nick   string `json:"nick" validate:"required,unick"`
	Pass   string `json:"pass" validate:"required,min=6"`
	Email  string `json:"email" validate:"required,email,umail"`
	Points int    `json:"points"`
	Rep    int    `json:"rep"`
}

func (u User) String() string {
	return fmt.Sprintf("ID: %s\nName: %s\nEmail: %s\nPoints: %d\nReputation: %d", u.Nick, u.Name, u.Email, u.Points, u.Rep)
}

// GenToken generates user token
//
func (u User) GenToken() string {
	return fmt.Sprintf("%s%s%s%s%d%d", u.Name, u.Nick, u.Pass, u.Email, u.Points, u.Rep)
}

// LoginUser is a struct used only for login
//
type LoginUser struct {
	Email string `json:"email" validate:"required"`
	Pass  string `json:"pass"  validate:"required,min=6"`
}

// SearchableUser used to search user
//
type SearchableUser struct {
	Email string `json:"email" validate:"required,email"`
	Pass  string `json:"pass"  validate:"required"`
}

// UserInfo is user information
//
type UserInfo struct {
	Name   string `json:"name" validate:"required"`
	Nick   string `json:"nick" validate:"required"`
	Email  string `json:"email" validate:"required,email"`
	Points int    `json:"points"`
	Rep    int    `json:"rep"`
}

// UpdatableUser used to update an existing user in database
//
type UpdatableUser struct {
	OldUser SearchableUser `json:"old_user" validate:"required"`
	NewUser User           `json:"new_user" validate:"required"`
}
