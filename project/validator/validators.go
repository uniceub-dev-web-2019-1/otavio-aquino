package validator

import (
	"otavio-aquino/project/db"
	"otavio-aquino/project/structs"
	"otavio-aquino/project/utils"
	"reflect"
	"strings"

	val "gopkg.in/go-playground/validator.v9"
)

// valid is the global validator
var valid *val.Validate

// Langs represents All suported languages
//
var Langs = []string{"Java", "Python", "go", "JavaScript", "html", "Ruby", "c", "c++", "php", "css", "c#", "React", "SQL"}

// CreateValidator 'configures' the 'Valid' validator
//
func CreateValidator() {
	valid = val.New()

	valid.RegisterValidation("language", validateLanguage)
	valid.RegisterValidation("unick", validateExists)
	valid.RegisterValidation("umail", validateExists)

	// Below functions make the Field function return tag json name instead of struct name
	//
	valid.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.Split(fld.Tag.Get("json"), ",")[0]
		if name == "-" {
			return ""
		}
		return name
	})
}

func validateLanguage(fl val.FieldLevel) bool {

	lang := (fl.Field().String())

	return utils.InSlice(Langs, lang)

}

func validateExists(fl val.FieldLevel) bool {
	field := fl.FieldName()
	value := fl.Field().String()
	user, _ := db.SearchUser(field, value)
	return user == structs.User{}
}

func errorsToMap(ve val.ValidationErrors) (errList []map[string]interface{}) {

	for _, fe := range ve {
		tag := fe.ActualTag()
		errList = append(errList, map[string]interface{}{
			"errorTag":       tag,
			"field":          fe.Field(),
			"value":          fe.Value(),
			"defaultMessage": defaultMsg(tag),
		})
	}

	return
}

func defaultMsg(tag string) string {
	switch tag {
	case "required":
		return "Must not be empty"

	case "email":
		return "E-mail is not valid"

	case "max":
		return "title can't have more than 50 characters"

	case "min":
		return "Password must have at least 6 characters"

	case "language":
		return "this language is not valid"

	case "unick":
		return "This nick is already in use"

	case "umail":
		return "This email is already in use"

	default:
		return "Error, something went wrong"
	}
}
