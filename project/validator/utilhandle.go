package validator

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"

	val "gopkg.in/go-playground/validator.v9"
)

// ValidateBody Reads and validates the body
//
func ValidateBody(body io.ReadCloser, item interface{}) (int, []map[string]interface{}, error) {

	if reflect.ValueOf(item).Kind() != reflect.Ptr {
		err := []map[string]interface{}{map[string]interface{}{"pointer": false}}
		return http.StatusInternalServerError, err, fmt.Errorf("The interface{} 'item' must be a pointer for validation")
	}

	byteBody, err := ioutil.ReadAll(body)
	if err != nil {
		return http.StatusBadRequest, nil, fmt.Errorf("Error Reading Body: [%v]", err)

	} else if err := json.Unmarshal(byteBody, item); err != nil {
		return http.StatusBadRequest, nil, fmt.Errorf("Error Parsing JSON: [%v]", err)

	} else if err = valid.Struct(item); err != nil {
		errorList := errorsToMap(err.(val.ValidationErrors))
		return http.StatusPreconditionFailed, errorList, fmt.Errorf("Invalid item creation because of: [%v]", err)

	} else {

		return http.StatusOK, nil, nil
	}

}
