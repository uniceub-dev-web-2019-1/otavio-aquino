package service

import (
	"log"
	"net/http"
	"otavio-aquino/project/db"
	"otavio-aquino/project/utils"
	"otavio-aquino/project/validator"
	"time"
)

//StartServer ...
//
func StartServer() {
	// declaração e atribuição do servidor http

	handler := createCors().Handler(creatRouter())

	server := http.Server{Addr: utils.ServerAddr,
		Handler:      handler,
		ReadTimeout:  100 * time.Millisecond,
		WriteTimeout: 200 * time.Millisecond,
		IdleTimeout:  50 * time.Millisecond}

	// Inicializa o Validador para ser usado nas requests
	validator.CreateValidator()

	// inicializa a conexão com o banco de dados
	err := db.CreateConection()

	if err != nil {
		log.Fatal(err)
	}

	// inicializar o servidor
	log.Print(server.ListenAndServe())
}

//  https://medium.com/@johnshenk77/create-a-simple-chat-application-in-go-using-websocket-d2cb387db836
