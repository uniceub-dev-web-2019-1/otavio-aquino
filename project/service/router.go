package service

import (
	"otavio-aquino/project/handle"
	"otavio-aquino/project/utils"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func creatRouter() (r *mux.Router) {

	r = mux.NewRouter()

	r.HandleFunc(utils.UserPath, handle.PostUserHandler).Methods("POST").HeadersRegexp("Content-Type", "app")
	//	r.HandleFunc(utils.UserPath, handle.PutUserHandler).Methods("PUT")
	r.HandleFunc(utils.LoginPath, handle.LoginUser).Methods("POST")
	r.HandleFunc("/langs", handle.ShowLangs).Methods("GET")

	r.HandleFunc(utils.CodePath, handle.PostCodeHandler).Methods("POST").HeadersRegexp("Content-Type", "application/json")
	r.HandleFunc(utils.CodePath, handle.PutCodeHandler).Methods("PUT").HeadersRegexp("Content-Type", "application/json")
	r.HandleFunc(utils.CodePath, handle.DelCodeHandler).Methods("DELETE").HeadersRegexp("Content-Type", "application/json")

	r.HandleFunc("/mycodes", handle.LookForCodes).Methods("GET")
	r.NotFoundHandler = handle.My404Handler()
	r.MethodNotAllowedHandler = handle.My405Handler()
	return

}

func createCors() *cors.Cors {
	return cors.New(cors.Options{
		AllowedOrigins: []string{"0.0.0.0", "http://localhost:3000", "localhost"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{"Content-Type", "Accept", "Content-Length", "Accept-Encoding", "Authorization", "X-CSRF-Token"},
	})

}
