function sSlog(msg, strStyle = "font-weight:bold") {
    setTimeout(console.log.bind(console, "%c" + msg, strStyle));
}

function zeroPad(num, digits=2) {
    let siz = String(num).length;
    return siz < digits ? `${"0".repeat(digits - siz)}${num}` : num;
}

function randInt(min=0, max=999) {
    return Math.floor((Math.random() * max) + min);
}

function isLogged(){
    if (localStorage.getItem('auth-token') === null || localStorage.getItem('email') === null || localStorage.getItem('nick') === null) return false;
    else return true;


}

export { isLogged, sSlog, randInt, zeroPad }; 