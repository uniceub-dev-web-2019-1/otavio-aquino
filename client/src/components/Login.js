import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { InputAdornment } from '@material-ui/core';
import ContactMail from '@material-ui/icons/ContactMail';
import Lock from '@material-ui/icons/Lock';
import { MAINPATH } from '../configs/consts.js';
import PubSub from 'pubsub-js';


function MadeWithLove() {

    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Built with love by the '}
            <Link color="inherit" to="#">
                StackBook
      </Link>
            {' team.'}
        </Typography>
    );
}

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        // marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        //marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function Login(props) {
    const [msg, setMsg] = React.useState('');
    const [mail, setMail] = React.useState('');
    const [pass, setPass] = React.useState('');

    const classes = useStyles();

    function sendLogin(event) {
        event.preventDefault();

        const url = `${MAINPATH}/login`;
        const data = JSON.stringify({
            email: mail,
            pass: pass
        });
        const head = {
            'Content-Type': 'application/json'
        }

        const reqInfo = {
            method: 'POST',
            body: data,
            headers: head
        };

        fetch(url, reqInfo).then(response =>{
            if(response.ok){
                return response.text();
            } else {
                console.log(response.status);
                
                throw new Error("Email ou senha inválidos");
            }
        }).then(respj =>{
            const usin = JSON.parse(respj);
            console.log(usin);
            
            localStorage.setItem('auth-token', usin.user.token);
            localStorage.setItem('email', usin.user.email);
            localStorage.setItem('nick',  usin.user.nick);
            props.history.push("/codes");
            PubSub.publish("path-change", '/codes');
        }).catch(error =>{

            setMsg(error.message);

            console.log(error);
        });
    }

    return (
        <Container component="main" maxWidth="xs">
            <div>
                <Typography gutterBottom variant="h5" color="error" component="h2">{msg}</Typography>
            </div>
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
        </Typography>
                <form className={classes.form} onSubmit={sendLogin}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth={true}
                        value={mail}
                        onChange={ev => setMail(ev.target.value)}
                        id="email"
                        label="Email Address"
                        name="email"
                        autoComplete="email"
                        autoFocus
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <ContactMail />
                                </InputAdornment>
                            )
                        }}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        value={pass}
                        onChange={ev => setPass(ev.target.value)}
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Lock />
                                </InputAdornment>
                            )
                        }}
                    />
                    <FormControlLabel
                        control={<Checkbox value="remember" color="primary" />}
                        label="Remember me"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign In
          </Button>
                    <Grid container>
                        <Grid item xs>
                            <Link to="#">
                                Forgot password?<br /> i'm so sorry
                            </Link>
                        </Grid>
                        <Grid item>
                            <Link to="/signup" variant="body2" >
                                {"Don't have an account? Sign Up"}
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <Box mt={5}>
                <MadeWithLove />
            </Box>
        </Container>
    );
}