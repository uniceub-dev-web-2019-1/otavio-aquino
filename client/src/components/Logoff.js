import React, { useState } from 'react';
import { makeStyles, MuiThemeProvider } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Typography, createMuiTheme } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import { MAINPATH } from '../configs/consts.js';
import { red } from '@material-ui/core/colors';



const useStyles = makeStyles(theme => ({
  button: {
    margin: 15,
  },
  input: {
    margin: theme.spacing(1),
  },
  form: {
    padding: 13,
  },
  myimg: {
    width: "100%",
    height: "auto",
  }

}));

const tm = createMuiTheme({
    palette:{
        primary: red
    }
});


export default function SignUp(props) {
    const classes = useStyles();
    function dealLogout(ev){
        ev.preventDefault();
        localStorage.removeItem('auth-token');
        localStorage.removeItem('email');
        localStorage.removeItem('nick');
        props.history.push('/');
        
    }
  return (

    <MuiThemeProvider theme={tm}>
    <Container component="main" maxWidth="sm">


      <div>
        <Typography gutterBottom variant="h5" component="p">Você tem certeza que quer realizar o logout?</Typography>
      </div>

      <form className={classes.form} onSubmit={dealLogout}>



        <Button variant="contained" label="Sair" type="submit" color="primary" className={classes.button}>Sair</Button>

      </form>
    </Container>
    </MuiThemeProvider>

  );

}