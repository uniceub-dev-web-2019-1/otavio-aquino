import React, { useState, useEffect } from 'react';
import { Container, TextField, Button, Typography, Select, InputLabel, FormControl, Paper, Divider, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { MAINPATH } from '../configs/consts.js';
import Search from '@material-ui/icons/Search';
import { isLogged } from '../util/util.js';
import { DeleteCodeDialog as CodeDel, EditCodeAlert as CodEdit } from './CodeUtils.js';
import {UnControlled as CodeMirror} from 'react-codemirror2';

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
         marginTop: theme.spacing(1),
        //    display: 'flex',
        //     flexDirection: 'column',
        //  alignItems: 'center',
        padding: theme.spacing(3, 2)
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        //marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    }, formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    }, input: {
        marginBottom: theme.spacing(1),
    }
}));

export default function Codes() {
    const classes = useStyles();
    const [codeL, setCodes] = useState([]);
    const [msg, setMsg] = useState('');
    const [langs, setLangs] = useState([]);

    function updateCodes(tCode){
        if(Array.isArray(tCode)){
            setCodes(tCode);
        } else  {
            const newCodes = codeL.concat(tCode);  
            setCodes(newCodes);
        }
    }

    const fetlangs = () => {
        const url = `${MAINPATH}/langs`;
        fetch(url).then(response => {
            if (response.ok) {
                return response.text();
            } else {
                throw new Error("Alguma coisa aconteceu");
            }

        }).then(body => setLangs(JSON.parse(body))).catch(error => setMsg(error.msg));

    };

    useEffect(() => {
        fetlangs();
        console.log('teste');
        const url = `${MAINPATH}/mycodes?nick=${localStorage.getItem('nick')}`;

        fetch(url).then(response => {
            if (response.ok) {
                return response.text();
            } else {
                return response.text();
            }

        }).then(body => {
            const bson = JSON.parse(body);
            if (bson != null) {
                setCodes(bson);

            } else setCodes([]);
        }
        ).catch(error => setMsg(error.message));

    }, []);




    return (
        <div>

            <CodeSearch cls={classes} langs={langs} update={updateCodes} style={{ float: 'left' }} />
            <Container>
                <Typography gutterBottom variant="h5" color="error" component="h2">{msg}</Typography>
                <CodeList up={updateCodes} langs={langs} cls={classes} codes={codeL} />

                <hr style={{ visibility: 'hidden' }} />
                <Divider component="hr" variant="middle" />
                <hr style={{ visibility: 'hidden' }} />

                <CodeForm langs={langs} updator={updateCodes} cls={classes} style={{ marginTop: "5px" }} />

            </Container>

        </div>
    );
}

function CodeSearch(props) {

    const [nick, setNick] = useState("");
    const [lang, setLang] = useState("");

    function realSearch(query) {

        console.log(nick);

        const url = `${MAINPATH}/mycodes?${query}`;

        fetch(url).then(response => {
            if (response.ok) {
                return response.text();
            } else {
                return response.text();
            }

        }).then(body => {
            console.log(body);
            if (JSON.parse(body) != null) {
                props.update(JSON.parse(body));
            } else {
                props.update([]);
            }
        }).catch(error => console.log(error.message));


    }


    const myCodes = (<Grid item md xs={12} lg sm>

        <form onSubmit={ev => { ev.preventDefault(); realSearch("nick=" + localStorage.getItem('nick')) }}>
            <Typography style={{ paddingBottom: '5px' }} variant="body2" color="textSecondary" component="p"></Typography>
            <FormControl>

                <Button type="submit" variant="contained">Meus códigos<Search /></Button>
            </FormControl>
        </form>
    </Grid>
    );


    return (
        <Grid container >
            {isLogged() ? myCodes : null}
            <Grid item md xs={12} lg sm>

                <form onSubmit={ev => { ev.preventDefault(); realSearch("nick=" + nick) }}>
                    <Typography style={{ paddingBottom: '5px' }} variant="body2" color="textSecondary" component="p">Pesquisar por usuários</Typography>
                    <FormControl>
                        <TextField required label="Username" value={nick} placeholder="Nick do usuário" onChange={ev => setNick(ev.target.value)} />
                        <Button type="submit"><Search /></Button>
                    </FormControl>
                </form>
            </Grid>
            <Grid item md xs={12} lg sm>

                <form onSubmit={ev => { ev.preventDefault(); realSearch("lang=" + lang) }}>
                    <Typography style={{ paddingBottom: '5px' }} variant="body2" color="textSecondary" component="p">Filtrar por linguagem</Typography>
                    <FormControl required className={props.cls.formControl}>
                        <InputLabel htmlFor="lang-simple">Language</InputLabel>
                        <Select
                            native
                            value={lang}
                            onChange={ev => setLang(ev.target.value)}
                            inputProps={{
                                required: true,
                                name: 'lang',
                                id: 'lang-simple',
                            }}
                        >
                            <option value="" disabled />


                            {
                                props.langs.map((name, index) => (
                                    <option key={name} value={name}>{name}</option>
                                ))
                            }

                        </Select>
                    </FormControl>
                    <Button type="submit"><Search /></Button>
                </form>
            </Grid>
        </Grid>
    );
}


function CodeList(props) {
    console.log(props.codes);
        
    return props.codes.map((code, index) =>{

       return <CodeField  codelist={props.codes} update={props.up} langs={props.langs} key={code.id} code={code} cls={props.cls}></CodeField>
    }
    )
}

function CodeField(props) {
    

    const btns = (<div>
        <div>
            <CodeDel clist={props.codelist} setList={props.update} code={props.code}  />
        </div>
        <div>
            <CodEdit clist={props.codelist} setList={props.update} langs={props.langs} code={props.code} cls={props.cls} />
        </div>
    </div>);

    function canUpdate() {
        if (isLogged()) {
            console.log(props.code);
            
            if (localStorage.getItem('email') === props.code.user.email && localStorage.getItem('nick') === props.code.user.nick) {
                return true;
            }
        }
        return false;
    }
const sc = {
    whiteSpace: "pre-wrap",
    wordWrap:"break-word",
    marginTop: "4px",
    overflow:"auto",
    textAlign:"left"
}
    return (
        <div style={{ paddingBottom: 50 }}>

            {canUpdate() ? btns : null}
            <Paper className={props.cls.paper}>
                <Typography style={{ paddingBottom: "5px" }} variant="h5" component="h2">
                    {props.code.title}
                </Typography>
                <Container style={{ backgroundColor: "lightgrey" }} m={1} maxWidth={"sm"}>

                    <Typography style={sc} component={'pre'}>

                        <code>
                            {props.code.code}
                        </code>

                    </Typography>
                </Container>

                <Grid container>
                    <Grid item xs sm lg md>Language: </Grid>
                    <Grid item xs sm lg md>{props.code.lang}</Grid>
                </Grid>
                <Grid container>
                    <Grid item xs sm lg md>Author: </Grid>
                    <Grid item xs sm lg md>{props.code.user.nick}</Grid>
                </Grid>
            </Paper>
        </div>
    );

}

function CodeForm(props) {
    const [code, setRCode] = useState("");
    const [lang, setRLang] = useState("");
    const [title, setRTitle] = useState("");
    

    const [values, setValues] = React.useState({
        code: '',
        lang: '',
        title: '',
    });

    function setTitle(event){
        setRTitle(event.target.value);        
    }

    function setLang(event){
        setRLang(event.target.value);
    }
        
    function setCode(event){
        setRCode(event.target.value);
    }


    const [msg, setMsg] = useState('');

    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };

    



    function sendFlemis(ev) {
        ev.preventDefault();
        const url = `${MAINPATH}/code`;
        const data = JSON.stringify({
            title: title,
            code: code,
            lang: lang,
            user: localStorage.getItem('email'),
            token: localStorage.getItem('auth-token')
        });
        const head = { "Content-Type": "application/json" };
        const reqi = {
            method: "POST",
            headers: head,
            body: data
        }

        fetch(url, reqi).then(response => {
            if (response.ok) {
                return response.text();
            } else {
                console.log(response.status);
                throw new Error("Verifique se está devidamente logado.");
            }
        }).then(body => {
            const jbody = JSON.parse(body);
            props.updator(jbody.code);

            setRCode("");          
            setRLang("");      
            setRTitle("");       

        }).catch(error => setMsg(error.message));

        console.log("submeteu");
    }

    // this function is used to prevent tab from switching focus;
    // 
    function dTab(e){
        if (e.keyCode == 9){
            e.preventDefault();
            e.target.value += "\t";
        }
    }


    return (
        <Container style={{ marginTop: '15px' }}>
            <div>
                <Typography gutterBottom variant="h5" color="error" component="h2">{msg}</Typography>
            </div>

            <div>
                <Typography gutterBottom variant="h5" color="primary" component="h2">{"Novo Código"}</Typography>
            </div>

            <form onSubmit={sendFlemis}>

                <TextField className={props.cls.input} required label="Title" value={title} onChange={setTitle} fullWidth={true} variant="outlined" />
                <TextField className={props.cls.input} required label="<code_here>" value={code} onChange={setCode} fullWidth={true} multiline variant="outlined" onKeyDown={dTab} />
                <CodeMirror style={{textAlign: 'left!important'}}
                    value="teste" options={{
                        mode:"python",
                        theme:"material",
                        lineNumbers:true
                    }}

                />
                <FormControl required className={props.cls.formControl}>
                    <InputLabel htmlFor="lang-simple">Language</InputLabel>
                    <Grid>

                    </Grid>
                    <Select
                        native
                        value={lang}
                        onChange={setLang}
                        inputProps={{
                            required: true,
                            name: 'lang',
                            id: 'lang-simple',
                        }}
                    >
                        <option value="" disabled />


                        {
                            props.langs.map((name, index) => (
                                <option key={name} value={name}>{name}</option>
                            ))
                        }

                    </Select>
                    <InputLabel htmlFor="lang-simple">Language</InputLabel>
                    <Select
                        native
                        value={lang}
                        onChange={setLang}
                        inputProps={{
                            required: true,
                            name: 'lang',
                            id: 'lang-simple',
                        }}
                    >
                        <option value="" disabled />


                        {
                            props.langs.map((name, index) => (
                                <option key={name} value={name}>{name}</option>
                            ))
                        }

                    </Select>
                </FormControl>
                <Container maxWidth='sm'>
                    <Button type="submit" fullWidth={false} variant="contained" color="primary" className={props.cls.submit}>Gravar Código</Button>
                </Container>
            </form>
        </Container>

    );

}