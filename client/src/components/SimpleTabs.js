import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import PubSub from 'pubsub-js';
import SBlogo from './SBLogo.js';
import {isLogged} from '../util/util.js';


function TabContainer(props) {
  return (
    <Typography component="div" style={{ paddingTop: 8 * 1, paddingBottom: 10 }}>
      {props.children}
    </Typography>
  );
}



TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
 //   backgroundColor: theme.palette.background.default,
  }
}));

function SimpleTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  
  /*
   * useLayoutEffect does the exact same thing but stops browser from rendering
   * if useEffect returns a function, this function will be executed in the same lifecycle than
   * componentWillUnmount, so be it, when it's time to 'clean' the component subscriptions if it has
   */
  
   React.useEffect(() => {
    
     setValue(initVal(window.location.pathname));
    PubSub.subscribe('path-change', (topic, newPath) => {
      setValue(initVal(newPath));
    });

    return () => {
      PubSub.unsubscribe('path-change');
    }


  }, []);



  // By default, the array passed as second parameters tells to execute when one of the elements passed as parameter changes, 
  // passing an empty array as second argument triggers the callback in useEffect only after the initial render thus replicating `componentDidMount`lifecycle behaviour
  // but it's different.
   


  function handleChange(_event, newValue) {
    setValue(newValue);
  }

  /* references
   *  https://material-ui.com/components/drawers/
   *  https://reactjs.org/docs/hooks-reference.html#uselayouteffect
   *  https://reactjs.org/docs/hooks-effect.html
   *  https://material-ui.com/api/app-bar/#appbar-api
   *  https://material-ui.com/components/app-bar/
   *  https://material-ui.com/components/tabs/
   *  https://stackoverflow.com/questions/37843495/material-ui-adding-link-component-from-react-router
   * 
   */


   // each tab is a button
  return (
    <div className={classes.root}>
      <AppBar position="static" color="primary">
        <Tabs value={value} onChange={handleChange} aadbjasbdhabdjs={"" /*variant="scrollable"*/} scrollButtons="on" centered >

          <Tab label="Home" component={Link} to="/" />
          <Tab label="Codes" component={Link} to="/codes" />
          <Tab label="SingUP" component={Link} to="/signup" />
          <Tab label={isLogged() ? "Logoff" : 'Login'} component={Link} to="/login" />
        </Tabs>
      </AppBar>
      
      {value === 0 && <TabContainer><h1>Welcome to our system</h1></TabContainer>}
      {value === 1 && <TabContainer><h2>Codes</h2></TabContainer>}
      {value === 2 && <TabContainer><SBlogo style={{paddingTop: 3, margin: 1}}/></TabContainer>}
      {value === 3 && <TabContainer><SBlogo style={{paddingTop: 3, margin: 1}}/></TabContainer>}


    </div>
  );
}



function initVal(path) {

  console.log(path);

  switch (path) {
    case '/':
      return 0;

    case '/codes':
      return 1;

    case '/signup':
      return 2;

    case '/login':
      return 3;
    default:
      return -1;
  }
}



export default SimpleTabs;