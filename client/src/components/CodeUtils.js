import React, { useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { Select, InputLabel, FormControl } from '@material-ui/core';
import { MAINPATH } from '../configs/consts.js';


export function DeleteCodeDialog(props) {
    const [open, setOpen] = React.useState(false);

    const [show, setShow] = React.useState(false);
    const [msg, setMsg] = React.useState(""); 

    function handleClickOpen() {
        setOpen(true);
    }

    function sendDelete() {
        const url = `${MAINPATH}/code`;
        const data = JSON.stringify({
            id: props.code.id,
            user: localStorage.getItem('email'),
            token: localStorage.getItem("auth-token")
        });
        const head = { "Content-Type": "application/json" };
        const reqinf = {
            method: "DELETE",
            headers: head,
            body: data
        }

        fetch(url, reqinf).then(response => {
            if (response.ok){
                return response.text();
            }else{
                console.log(response.status);
                const tbool = response.status === 401 || response.status === 403;
                console.log(typeof(response.status));
                const err = tbool ? "Você não está autorizado a realizar essa ateração!" : "Algum erro ocorreu, não foi possível deletar";
                
                throw new Error(err);
            }
        }).then(body => {
            const jbody = JSON.parse(body);

            
            const nlist = props.clist.concat();
            const ind = nlist.findIndex(cd => cd.id === jbody.oldcode.id);

            nlist.splice(ind, 1);
            props.setList(nlist);
            setOpen(false);
        }).catch(error =>{
            setMsg(error.message);
            setShow(true);
        });
    }

    function cancel() {
        setOpen(false);
    }

    function confirm() {
        sendDelete();

    }

    function closeInternalAlert(){
        setShow(false);
    }

    return (
        <div>
            <Button variant="contained" color="secondary" onClick={handleClickOpen}>
                Deletar
        </Button>
        <AlertDialog title="Não foi possível atualizar" open={show} close={closeInternalAlert} msg={msg}/>
            <Dialog
                open={open}
                onClose={cancel}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{`Tem certeza que deseja deletar o código?`}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Uma vez que o código for deletado, não haverá como recuperar.
            </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button variant={"contained"} onClick={cancel}  color="primary">Cancelar</Button>
                    <Button variant={"contained"} onClick={confirm} color="secondary" autoFocus>Deletar</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export function EditCodeAlert(props) {
    const [open, setOpen] = React.useState(false);

    const [code, setCode] = React.useState(props.code.code);
    const [titl, setTitl] = React.useState(props.code.title);
    const [lang, setLang] = React.useState(props.code.lang);

    const [show, setShow] = React.useState(false);
    const [msg, setMsg] = React.useState(""); 

    function handleClickOpen() {
        setOpen(true);
    }

    function cancel() {
        setCode(props.code.code);
        setTitl(props.code.title);
        setLang(props.code.lang);
        setOpen(false);
    }

    function sendUpdate() {
        console.log(props.code.id);
        
        const url = `${MAINPATH}/code`;
        const data = JSON.stringify({
            id: props.code.id,
            title: titl,
            lang: lang,
            code: code,
            user: localStorage.getItem('email'),
            token: localStorage.getItem("auth-token")
        });

        
        const head = { "Content-Type": "application/json" };
        const reqinf = {
            method: "PUT",
            headers: head,
            body: data
        }

        fetch(url, reqinf).then(response => {
            if (response.ok){
                return response.text();
            }else{
                console.log(response.status);
                const tbool = response.status === 401 || response.status === 403;
                console.log(typeof(response.status));

                const err = tbool ? "Você não está autorizado a realizar essa ateração!" : "Algum erro ocorreu, não foi possível deletar";
                
                throw new Error(err);
            }
        }).then(body => {
            const jbody = JSON.parse(body);

            const nlist = props.clist.concat();
            const ind = nlist.findIndex(oldCode => {
                console.log(oldCode);
                

                return oldCode.id === jbody.code.id;

            });
            nlist[ind] = jbody.code;
            
            props.setList(nlist);

            setOpen(false);
        }
            ).catch(error => {

                setMsg(error.message);
                setShow(true);
            });
    }

    function confirm() {
        sendUpdate();
    }
    function closeInternalAlert(){
        setShow(false);
    }

    return (
        <div>
            <Button variant="contained" color="primary" onClick={handleClickOpen}>Editar</Button>
            <form>
                <AlertDialog title="Não foi possível atualizar" open={show} close={closeInternalAlert} msg={msg}/>

                <Dialog open={open} onClose={cancel} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Editar</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Edite o código e confirme as alterações!
            </DialogContentText>
                        <TextField autoFocus value={titl}
                            margin="dense"
                            required
                            id="title"
                            label="Title"
                            type="text"
                            onChange={event => setTitl(event.target.value)}
                            fullWidth
                        />
                        <TextField
                            required
                            value={code}
                            margin="dense"
                            variant={"filled"}
                            id="name"
                            label="Code"
                            multiline
                            type="email"
                            fullWidth
                            onChange={event => setCode(event.target.value)}
                        />
                        <FormControl required className={props.cls.formControl}>
                        <InputLabel htmlFor="lang-simple">Language</InputLabel>
                        <Select
                        required
                            native
                            value={lang}
                            onChange={event => setLang(event.target.value)}
                            inputProps={{
                                required: true,
                                name: 'lang',
                                id: 'lang-simple',
                            }}
                        >

                            {
                                props.langs.map((name, index) => (
                                    <option key={name} value={name}>{name}</option>
                                ))
                            }

                        </Select>
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <Button variant={"contained"} onClick={cancel} color="secondary">Cancelar</Button>
                        <Button variant={"contained"} onClick={confirm} type={"submit"} color="primary">Confirmar</Button>
                    </DialogActions>
                </Dialog>
            </form>
        </div>
    );
}




function AlertDialog(props) {
    const [open, setOpen] = React.useState(false);
    
    useEffect(() =>{
        setOpen(props.open);
    }, [props.open]);

  
    function close() {
      props.close();
    }
  
    return (
      <div>
        
        <Dialog
          open={open}
          onClose={close}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle style={{color:'red'}} id="alert-dialog-title">{props.title}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {props.msg}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={close} color="primary">OK</Button>
 
          </DialogActions>
        </Dialog>
      </div>
    );
  }