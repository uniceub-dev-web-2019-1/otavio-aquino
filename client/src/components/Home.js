import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Card, CardContent, CardActionArea, Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import Paper from '@material-ui/core/Paper';
import { Link } from 'react-router-dom';
import codeimg from "../img/mcode.jpg";
import singimg from "../img/singp.jpg";
import lognimg from "../img/login.png";
import StackBook from './SBLogo';



export default function Home() {


    return (
        <div>
            <Box>
                <HomeGrid />
                <PaperSheet />
            </Box>

        </div>);


}


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
        paddingBottom: theme.spacing(3)
    },
    gridList: {
        width: 500,
        height: 450,
    },
    media: {
        height: 140,
    },
    card: {
        maxWidth: 345,
    },
    paper: {
        padding: theme.spacing(3, 2)
    }
}));

const HomeGrid = (props => {
    const classes = useStyles();


    return (
        <div className={classes.root}>


            <Grid container style={{ flexGrow: 1 }} className={classes.root} spacing={2} >

                <Grid item xs={12} lg={4}>
                    <Card className={classes.card}>
                        <CardActionArea to="/codes" component={Link}>

                            <CardMedia component={'img'} className={classes.media} src={codeimg} title="Code Card" />

                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">Codes</Typography>
                                <Typography variant="body2" color="textSecondary" component="p">Aqui você poderá escrever códigos e compartilhar com seus amigos!</Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Grid>

                <Grid item xs md sm lg={4}>
                    <Card className={classes.card}>
                        <CardActionArea to="/signup" component={Link}>

                            <CardMedia component={'img'} className={classes.media} src={singimg} title="Sing Up Card" />

                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">Sign Up</Typography>
                                <Typography variant="body2" color="textSecondary" component="p">Ainda Não criou a sua conta? Cadastre-se logo!!!</Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Grid>

                <Grid item xs={12} lg={4}>
                    <Card className={classes.card}>
                        <CardActionArea to="/login" component={Link} >

                            <CardMedia component={'img'} className={classes.media} src={lognimg} title="Login Card" />

                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">Login</Typography>
                                <Typography variant="body2" color="textSecondary" component="p">Já está cadastrado? realize o login e começe a se divertir!</Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Grid>

            </Grid>
        </div>
    );
});



function PaperSheet() {
    const classes = useStyles();

    return (
        <div>
            <Paper className={classes.paper}>
                <Typography variant="h5" component="h2">
                    <StackBook/>
                 </Typography>

                <Typography component="p">
                   O projeto StackBook foi criado em 1930, por Albert Einstein.<br/>
                   Muitas pessoas se juntaram para colaborar com este incrível projeto!
                </Typography>
            </Paper>
        </div>
    );
}