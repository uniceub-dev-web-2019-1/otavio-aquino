import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Typography } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import { MAINPATH } from '../configs/consts.js';


const useStyles = makeStyles(theme => ({
  button: {
    margin: 15,
  },
  input: {
    margin: theme.spacing(1),
  },
  form: {
    padding: 13,
  },
  myimg: {
    width: "100%",
    height: "auto",
  }

}));


export default function SignUp(props) {
  const [name, setRName] = useState('');
  const [nick, setRNick] = useState('');
  const [pass, setRPass] = useState('');
  const [mail, setRMail] = useState('');
  const [tmsg, setSTmsg] = useState('');


  function setName(event) {
    setRName(event.target.value);
  }

  function setNick(event) {
    setRNick(event.target.value);
  }

  function setPass(event) {
    setRPass(event.target.value);
  }

  function setMail(event) {
    setRMail(event.target.value);
  }



  const classes = useStyles();



  const sendRegister = (event) => {
    event.preventDefault();
    const url = `${MAINPATH}/user`;
    const data = JSON.stringify({
      'name': name,
      'email': mail,
      'nick': nick,
      'pass': pass
    });

    const head = {
      'Content-Type': 'application/json'
    }

    const rinf = {
      method: 'POST',
      body: data,
      headers: head
    };

    fetch(url, rinf).then(response => {
      if (response.ok) {
        return response.text();
      } else {
        console.log(response.status);
        
        throw new Error("Por favor verifique se todos os campos estão devidamente preenchidos");
      }
    }).then(respj => console.log(respj)).catch(error =>{
      setSTmsg(error.message);
      console.log(error);
      
    });
    



  }


  // const classes = this.style();

  
  return (


    <Container component="main" maxWidth="sm">
      <div>
        <Typography gutterBottom variant="h5" color="error" component="h2">{tmsg}</Typography>
      </div>

      <div>
        <Typography gutterBottom variant="h5" component="h2">Criar sua conta</Typography>
      </div>

      <form className={classes.form} onSubmit={sendRegister}>


        <TextField fullWidth={true} className={classes.input} variant="outlined" value={name} required={true}
          placeholder="Type your name" label="Name" type="text" onChange={setName} />

        <TextField fullWidth={true} className={classes.input} variant="outlined" value={nick} required={true}
          placeholder="Type your nick" label="Nick" type="text" onChange={setNick} />

        <MyInput fullWidth={true} className={classes.input} variant="outlined" value={mail} required={true}
          placeholder="Type your email" label="E-mail" type="email" onChange={setMail} />

        <TextField fullWidth={true} className={classes.input} variant="outlined" value={pass} required={true}
          placeholder="Type your password" label="Password" type="password" onChange={setPass} />


        <Button variant="contained" label="Submit" type="submit" color="primary" className={classes.button}>Submit</Button>

      </form>
    </Container>

  );

}


function MyInput(props){
 return(<div>

   <TextField {...props} fullWidth={props.fullWidth} variant={props.variant} /> <span>teste</span>
 </div>
 );
       //   placeholder="Type your email" label="E-mail" type="email" onChange={setMail} />
}