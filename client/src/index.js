import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'semantic-ui-css/semantic.min.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import logo from './logo.svg';
import SignUp from './components/SignUp.js';
import Login from './components/Login';
import Home from './components/Home.js';
import Codes from './components/Codes.js';
import { isLogged }  from './util/util.js';
import Logout from './components/Logoff.js';
import Bab from './components/Header.js';


ReactDOM.render(

    
    <BrowserRouter>
        
        <App>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/login" render={props => isLogged() ? <Logout {...props}/>: <Login {...props}/>} />
                <Route exact path="/codes" component={Codes}/>
                <Route exact path="/signup" component={SignUp}/>
                <Route exact path="/logoff" component={Logout}/>
                <Route path="*" component={MeuCompNotFound}></Route>
            </Switch>
        </App>
    </BrowserRouter>

    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();





function MeuCompNotFound() {
    return ((
        <div>
            <div>
                <div className="header">
                    <h1>404 Not found</h1>
                </div>

                <div className="content" id="content" style={{ marginTop: '5px' }}>
                    <div style={{ textAlign: "center" }}>

                        <p><span className="er404" style={{ textAlign: "center", color: 'red' }}>404 | </span> Página não encontrada.</p>
                        <img src={logo} className="App-logo" alt="404 Logo"></img>
                        <p>A página que você está tentando acessar está indisponível, ou não existe</p>
                    </div>
                </div>
            </div>

        </div>
    ));
}



export function NotYet() {
    return ((
        <div>
            <div>
                <div className="header">
                    <h1>501 Not Implemented</h1>
                </div>

                <div className="content" id="content" style={{ marginTop: '5px' }}>
                    <div style={{ textAlign: "center" }}>

                        <p><span className="er404" style={{ textAlign: "center", color: 'red' }}>404 | </span> Página não encontrada.</p>
                        <img src={logo} className="App-logo" alt="404 Logo"></img>
                        <p>A página que você está tentando acessar está indisponível, ainda não foi implementada</p>
                    </div>
                </div>
            </div>

        </div>
    ));
}