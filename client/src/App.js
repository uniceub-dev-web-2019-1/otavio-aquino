import React from 'react';
import './App.css';
import SimpleTabs from './components/Header.js';
import Container from '@material-ui/core/Container';
import {withRouter} from 'react-router-dom';
import PubSub from 'pubsub-js';

require('codemirror/lib/codemirror.css');
require('codemirror/theme/material.css');
require('codemirror/theme/neat.css');
require('codemirror/mode/xml/xml.js');
require('codemirror/mode/javascript/javascript.js');
require('codemirror/mode/python/python.js');


function App(props) {
  
  React.useEffect(()=>{
    return  props.history.listen( (location, _action) => {

      PubSub.publish("path-change", location.pathname);
      
    });

    
  });


    return (
      <div className="App duv" >
        
        <SimpleTabs>
        <Container fixed maxWidth='lg'>

        {props.children}
        </Container>
        </SimpleTabs>


        {/* <div className="btnCont">
      <button className="litbut" onClick={dealBut}>Click to POST</button>
    </div> */}
      </div>


    );
  }

  export default withRouter(App);




    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
